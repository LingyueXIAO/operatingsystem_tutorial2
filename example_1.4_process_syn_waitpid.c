#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

int main()
{
	printf("This is the beginning of the program!\n\n");
	pid_t fpid;   //fpid, return value of fork function
	
	pid_t pid_wait;
	int status;
	
	fpid = fork();
	if (fpid < 0)
		printf("Error in executing fork!");
	else if (fpid == 0)
	{
		printf("I am the child process with ID %d. \n\n", getpid());
		sleep(5);
		exit(2);
	}
	else
	{
		pid_wait=waitpid(fpid, &status, WNOHANG); // with para WNOHANG, process won't be blocked
		while(pid_wait==0)
		{
			printf("The ID of terminated process is %d.\n", fpid); 
			pid_wait=waitpid(fpid, &status, WNOHANG); 
			//pid_wait=wait(&status);
			if(pid_wait==0)
			{
				printf("There is no process exited.\n");
			}
			sleep(1);
		}
		printf("The exit code of terminated process is %d.\n", WEXITSTATUS(status)); 
		printf("The ID of terminated process is %d.\n", pid_wait); 
		printf("I am the parent process with ID %d. \n", getpid());	
	}

	printf("This is the END of grogram.\n\n");
	return 0;
}

