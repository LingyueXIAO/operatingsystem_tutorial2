#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>


int main()
{
	printf("This is the beginning of the program!\n\n");

	for(int i=0;i<3;i++)
	{
		pid_t fpid=fork();
		if (fpid < 0)
			printf("Error in executing fork!");
		else if (fpid == 0)
		{
			continue;
		}
		else
		{
			wait(NULL);
		}
	}
	
	printf("This is the END of grogram. \n\n");
	return 0;
}
