#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

int main()
{
	printf("This is the beginning of the program!\n\n");
	int status;
	
	for(int i=0;i<5;i++)
	{	
		pid_t fpid;   //fpid, return value of fork function
		fpid = fork();
		if (fpid < 0)
			printf("Error in executing fork!\n");
		else if (fpid == 0){
			exit(i);
		}
	}
	
	for(int i=0;i<5;i++){
		wait(&status);
		printf("Exit information is %d.\n\n",WEXITSTATUS(status));
	}
	
	printf("This is the END of grogram.\n\n");
	return 0;
}
